"""
Save rotations and vertical/horizontal flips of all images in a given folder. Images should be 2D (not 3D stacks)
Last tested on Python 3.8, PyJAMAS 2021.3.3
"""

from pyjamas.rplugins.base import PJSPluginABC
from PyQt5 import QtCore, QtWidgets, QtGui
import os
import numpy as np
from pyjamas.rutils import RUtils
from pyjamas.rimage.rimutils import rimutils 

class PJSPlugin(PJSPluginABC):
    def name(self) -> str:
        return "Rotate, flip and save copies"

    def run(self, parameters: dict):
        inputfolder = QtWidgets.QFileDialog.getExistingDirectory(None, 'Get images', os.path.abspath(self.pjs.cwd))
        if inputfolder == '' or inputfolder is False:
            return False        
        filelist = [file for file in os.listdir(inputfolder) if os.path.splitext(file)[1].lower() == ".tif"]
        if len(filelist) == 0:
            return False
        filelist.sort(key=RUtils.natural_sort)

        for file in filelist:
            print('Processing ' + file)
            filename, ext = os.path.splitext(file)
            image = rimutils.read_stack(os.path.join(inputfolder, file))
            rimutils.write_stack(os.path.join(inputfolder, filename + "_rot1" + ext), np.rot90(image, 1))
            rimutils.write_stack(os.path.join(inputfolder, filename + "_rot2" + ext), np.rot90(image, 2))
            rimutils.write_stack(os.path.join(inputfolder, filename + "_rot3" + ext), np.rot90(image, 3))
            rimutils.write_stack(os.path.join(inputfolder, filename + "_flipud" + ext), np.flipud(image))
            rimutils.write_stack(os.path.join(inputfolder, filename + "_fliplr" + ext), np.fliplr(image))
                    
        return True