"""
Save a 100 by 100 px window around all fiducials in the current image stack and in a corresponding image stack. Please modify the folder paths and name changes for corresponding images before use!
Last tested on Python 3.8, PyJAMAS 2021.3.3
"""
from pyjamas.rplugins.base import PJSPluginABC
from pyjamas.pjscore import PyJAMAS
from pyjamas.rimage.rimutils import rimutils
from PyQt5 import QtWidgets
import os

class PJSPlugin(PJSPluginABC):
    def name(self) -> str:
        return "Save window around fiducials"

    def run(self, parameters: dict) -> bool:
        #I flipped the target and source but haven't renamed the variables, keep that in mind!
        current_filename = self.pjs.filename
        current_image = self.pjs.slices
        savefolder_current = "D:/imagedata/ecadh_resille/target" #edit this 

        corr_filename = current_filename.casefold().replace("w2c561", "w1c488") #edit this
        print(current_filename, corr_filename)
        corr_image = rimutils.read_stack(corr_filename)
        savefolder_corr = "D:/imagedata/ecadh_resille/source" #edit this

        base = os.path.splitext(os.path.basename(current_filename))[0]
        print(base)
        
        for i in range(len(current_image)):
            for fid in self.pjs.fiducials[i]:
                print(i, fid[0], fid[1])
                current_path = os.path.join(savefolder_current, base+"_"+str(i)+"_"+str(fid[0])+"_"+str(fid[1])+".tif")
                corr_path = os.path.join(savefolder_corr, base+"_"+str(i)+"_"+str(fid[0])+"_"+str(fid[1])+".tif")

                try:
                    rimutils.write_stack(current_path, current_image[i, fid[1]-50:fid[1]+50, fid[0]-50:fid[0]+50]) #change +/- 50 to another integer for different sized patches
                    rimutils.write_stack(corr_path, corr_image[i, fid[1]-50:fid[1]+50, fid[0]-50:fid[0]+50])
                except Exception as e:
                    print(e)
                    
                print('saved source to', current_path)
                print('saved target to', corr_path)
                
        return True
