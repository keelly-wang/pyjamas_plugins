"""
Generate meshes from annotations: Given the below folder structure with annotation files, generate 3D meshes of each cell, which can then be opened in the 3DViewer plugin.

Folder structure for annotation files:
--> top_level_folder (set input folder to this in GUI)
    --> timepoint1
        --> cell_1.pjs
        --> cell_2.pjs
    --> timepoint2
        --> cell_1.pjs
        --> cell_2.pjs
    --> timepoint3

The returned folder structure of meshes will match this.

Last tested on Python 3.8, PyJAMAS 2021.3.3
"""

from pyjamas.rplugins.base import PJSPluginABC
from pyjamas.rutils import RUtils
from pyjamas.pjscore import PyJAMAS
from PyQt5 import QtWidgets, QtCore
import os 
import numpy as np
from matplotlib.path import Path
from skimage import measure, filters

class PJSPlugin(PJSPluginABC):
    def name(self) -> str:
        return "Create meshes from annotations"

    def run(self, parameters: dict) -> bool:
        xsize, ysize, zsize = (512, 512, 45)
        dialog = QtWidgets.QDialog()
        self.setupUI(dialog)
        dialog.exec()

        importfolder = self.input_folder_name.text()
        savefolder = self.output_folder_name.text()
        z_repeat = self.z_height.value()

        # GET SUBFOLDERS 
        folderlist = next(os.walk(importfolder))[1]

        if len(folderlist) > 0: #for inputfolder with timepoint subfolders
            folderlist.sort(key=RUtils.natural_sort)
            print("timepoint folders:", *folderlist)

        else: #if inputfolder has no subfolders, but have annotations itself
            print("no subfolders, assume current folder contains all annotations")
            folderlist = [""] 

        # POINTS FOR matplotlib.path.contains_points()
        x, y = np.meshgrid(np.arange(xsize), np.arange(ysize))
        x, y = x.flatten(), y.flatten()
        allpoints = np.vstack((x,y)).T

        # PROCESSING EACH TIMEPOINT
        for folder in folderlist: 
            filelist = [file for file in os.listdir(os.path.join(importfolder, folder)) if os.path.splitext(file)[1].lower() == ".pjs"]
            filelist.sort(key=RUtils.natural_sort)

            try:
                os.mkdir(os.path.join(savefolder, folder))
            except FileExistsError: #if that directory name already exists, clears that directory
                curfiles = os.listdir(os.path.join(savefolder, folder))
                for f in curfiles:
                    os.remove(os.path.join(savefolder, folder, f))
                
            for filename in filelist:
                data = np.zeros((xsize, ysize, zsize))
                filepath = os.path.join(importfolder, folder, filename)
                self.pjs.io.cbLoadAnnotations([filepath], image_file = "") #loads annotation

                for j in range(self.pjs.n_frames):
                    if len(self.pjs.polylines[j]) > 0:
                        polypoints = np.array([[point.x(), point.y()] for point in self.pjs.polylines[j][0]])

                        p = Path(polypoints, closed=True)
                        mask = p.contains_points(allpoints).reshape(xsize, ysize) #creates a 3D array with all points inside cell being 1 (3D volume)
                        data[:, :, j][mask] = 1

                data = filters.gaussian(data, sigma=1, multichannel=False) #smoothening
                data = np.repeat(data, repeats=z_repeat, axis=2)

                verts, faces, _, _ = measure.marching_cubes(data, 0.5)  #generate surface mesh from 3D array

                savename = os.path.splitext(filename)[0] + ".npz"
                np.savez(os.path.join(savefolder, folder, savename), verts=verts, faces=faces) #saving mesh
                print('Processed', filename)

            print(len(filelist), "stacks processed in folder", folder)           
                    
        return True

    def setupUI(self, dialog):
        dialog.resize(342, 183)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(dialog.sizePolicy().hasHeightForWidth())
        dialog.setSizePolicy(sizePolicy)
        dialog.setWindowTitle("Create meshes from annotations")

        self.buttonBox = QtWidgets.QDialogButtonBox(dialog)
        self.buttonBox.setGeometry(QtCore.QRect(-87, 140, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")

        self.input_folder_name = QtWidgets.QLineEdit(dialog)
        self.input_folder_name.setGeometry(QtCore.QRect(204, 20, 113, 21))
        self.input_folder_name.setObjectName("input_folder_name")
        self.input_folder_name.setText(os.path.abspath(self.pjs.cwd))

        self.output_folder_name = QtWidgets.QLineEdit(dialog)
        self.output_folder_name.setGeometry(QtCore.QRect(204, 60, 113, 21))
        self.output_folder_name.setObjectName("output_slices")
        self.output_folder_name.setText(os.path.abspath(self.pjs.cwd))
        
        self.z_height = QtWidgets.QSpinBox(dialog)
        self.z_height.setGeometry(QtCore.QRect(204, 98, 48, 21))
        self.z_height.setObjectName("z_height")
        self.z_height.setMinimum(1)
        self.z_height.setMaximum(10)
        self.z_height.setValue(3)

        self.label = QtWidgets.QLabel(dialog)
        self.label.setGeometry(QtCore.QRect(25, 23, 81, 16))
        self.label.setObjectName("label")
        self.label.setText("input folder")

        self.label_2 = QtWidgets.QLabel(dialog)
        self.label_2.setGeometry(QtCore.QRect(25, 63, 151, 16))
        self.label_2.setObjectName("label_2")
        self.label_2.setText("output folder")

        self.label_3 = QtWidgets.QLabel(dialog)
        self.label_3.setGeometry(QtCore.QRect(25, 102, 151, 16))
        self.label_3.setObjectName("label_3")
        self.label_3.setText("z-stretch factor")

        self.btn_input_folder = QtWidgets.QToolButton(dialog)
        self.btn_input_folder.setGeometry(QtCore.QRect(175, 20, 26, 22))
        self.btn_input_folder.setObjectName("btn_input_folder")
        self.btn_input_folder.clicked.connect(self.open_input_dialog)

        self.btn_output_folder = QtWidgets.QToolButton(dialog)
        self.btn_output_folder.setGeometry(QtCore.QRect(175, 60, 26, 22))
        self.btn_output_folder.setObjectName("btn_output_folder")
        self.btn_output_folder.clicked.connect(self.open_output_dialog)

        self.buttonBox.rejected.connect(dialog.reject)
        self.buttonBox.accepted.connect(dialog.accept)
        QtCore.QMetaObject.connectSlotsByName(dialog)
    
    def open_input_dialog(self) -> bool:
        folder = QtWidgets.QFileDialog.getExistingDirectory(None, 'Get annotations', self.input_folder_name.text())
        if folder == '' or folder is False:
            return False
        self.input_folder_name.setText(folder)

    def open_output_dialog(self) -> bool:
        folder = QtWidgets.QFileDialog.getExistingDirectory(None, 'Get annotations', self.output_folder_name.text())
        if folder == '' or folder is False:
            return False
        self.output_folder_name.setText(folder)



