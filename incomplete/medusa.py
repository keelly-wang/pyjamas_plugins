"""
the GUI and the layout of the plugin is done, but something is going wrong in MedusaSegmentation. I have never been able to get MEDUSA in SIESTA working on my own computer, and I was never able to troubleshoot the issue.
"""
from PyQt5 import QtCore, QtWidgets, QtGui
from pyjamas.rplugins.base import PJSPluginABC
from pyjamas import dialogs
import numpy
from skimage import segmentation
from pyjamas.rimage.rimutils import rimutils
from scipy import ndimage, interpolate
from matplotlib import path
import pyjamas.rannotations.rpolyline as rpolyline

class PJSPlugin(PJSPluginABC):
    kappa_x = 0.4
    kappa_y = 0.4
    beta = 5.00
    window_size = 100
    sigma_gradient = None
    area_tolerance = 25

    def name(self) -> str:
        return "MEDUSA segmentation"

    def run(self, parameters: dict, firstSlice = None, lastSlice = None, kappaX = None, kappaY = None, beta = None, 
                win_size = None, sigmaGradient = None, areaTol = None):

        #open up a dialog
        if (firstSlice is False or firstSlice is None or lastSlice is False or lastSlice is None or
            kappaX is False or kappaX is None or kappaY is False or kappaY is None or beta is False or
            beta is None or win_size is False or win_size is None or sigmaGradient is False or sigmaGradient is None 
            or areaTol is False or areaTol is None) and self.pjs is not None:

            dialog = QtWidgets.QDialog()
            
            if self.pjs.n_frames == 1:
                lastSlice = 1
            else:
                lastSlice = self.pjs.slices.shape[0]
            
            mean_in = numpy.mean(self.pjs.slices)
            if mean_in < 500:
                PJSPlugin.sigma_gradient = 1
            elif mean_in >= 500 and mean_in < 2000:
                PJSPlugin.sigma_gradient = 30
            elif mean_in >= 2000 and mean_in < 12500:
                PJSPlugin.sigma_gradient = 60
            elif mean_in >= 12500 and mean_in < 22000:
                PJSPlugin.sigma_gradient = 100
            else:
                PJSPlugin.sigma_gradient=200

            #setup UI with the one added on in the slice numbers, but return normal please
            self.setupUI(dialog, firstslice=self.pjs.curslice + 1, lastslice=lastSlice,
                        kappax=PJSPlugin.kappa_x,
                        kappay=PJSPlugin.kappa_y,
                        beta=PJSPlugin.beta,
                        win_size=PJSPlugin.window_size,
                        sigma=PJSPlugin.sigma_gradient,
                        areatolerance=PJSPlugin.area_tolerance)

            dialog.exec()
            # If the dialog was closed by pressing OK, then run the measurements.
            continue_flag = dialog.result() == QtWidgets.QDialog.Accepted
            dialog.close()

        # Otherwise, continue with the supplied parameters.
        else:
            theparameters = {'first': firstSlice, 'last': lastSlice, 'kappa_x': kappaX, 'kappa_y': kappaY,
                                'beta': beta, 'window_size': win_size, 'sigma_gradient': sigmaGradient,
                                'area_tolerance': areaTol}
            continue_flag = True

        if continue_flag: #check this
            theparameters = self.parameters()
            print("the parameters are", theparameters)

            if len(self.pjs.polylines[theparameters['first']]) < 1:
                print('No polygons defined on time point', str(theparameters['first'] + 1))
                return False

            if theparameters['first'] <= theparameters['last']:
                theslicenumbers = numpy.arange(theparameters['first'], theparameters['last'])
            else:
                theslicenumbers = numpy.arange(theparameters['first'], theparameters['last'] - 1, -1)
                #check this, it's probably wrong
            
            print("the slices are", theslicenumbers)
            
            self.MEDUSASegmentation(theparameters, theslicenumbers)
        
        return continue_flag

    def setupUI(self, Dialog, firstslice=None, lastslice=None, kappax=None, kappay=None, beta=None, win_size=None, 
                sigma=None, areatolerance=None):
        
        # Parameter error control, not implemented right now but IT WILL BE IMPLEMENTED LATER

        #GUI stuff
        Dialog.setObjectName("Dialog")
        Dialog.resize(500, 310)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setWindowTitle("MEDUSA")

        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(20, 250, 180, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")

        self.groupBox_1 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_1.setGeometry(QtCore.QRect(20, 20, 180, 120))
        self.groupBox_1.setObjectName("groupBox_1")
        self.groupBox_1.setTitle("Slices")

        self.sbLast = QtWidgets.QSpinBox(self.groupBox_1)
        self.sbLast.setGeometry(QtCore.QRect(100, 70, 48, 24))
        self.sbLast.setMinimum(1)
        self.sbLast.setMaximum(lastslice)
        self.sbLast.setValue(lastslice)
        self.sbLast.setObjectName("sbLast")
        self.sbFirst = QtWidgets.QSpinBox(self.groupBox_1)
        self.sbFirst.setGeometry(QtCore.QRect(100, 25, 48, 24))
        self.sbFirst.setMinimum(1)
        self.sbFirst.setMaximum(lastslice)
        self.sbFirst.setValue(firstslice)
        self.sbFirst.setObjectName("sbFirst")

        self.label_2 = QtWidgets.QLabel(self.groupBox_1)
        self.label_2.setGeometry(QtCore.QRect(38, 70, 53, 24))
        self.label_2.setObjectName("label_2")
        self.label_2.setText("last slice")
        self.label = QtWidgets.QLabel(self.groupBox_1)
        self.label.setGeometry(QtCore.QRect(35, 25, 56, 24))
        self.label.setObjectName("label")
        self.label.setText("first slice")

        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setGeometry(QtCore.QRect(220, 20, 260, 260))
        self.groupBox_2.setObjectName("groupBox_2")
        self.groupBox_2.setTitle("Parameters")

        self.leKappaX = QtWidgets.QLineEdit(self.groupBox_2)
        self.leKappaX.setGeometry(QtCore.QRect(190, 25, 48, 24))
        self.leKappaX.setText(str(kappax))
        self.leKappaX.setObjectName("leKappaX")

        self.label_3 = QtWidgets.QLabel(self.groupBox_2)
        self.label_3.setGeometry(QtCore.QRect(38, 25, 153, 24))
        self.label_3.setObjectName("label_3")
        self.label_3.setText("kappa x")

        self.leKappaY = QtWidgets.QLineEdit(self.groupBox_2)
        self.leKappaY.setGeometry(QtCore.QRect(190, 65, 48, 24))
        self.leKappaY.setText(str(kappay))
        self.leKappaY.setObjectName("leKappaY")

        self.label_4 = QtWidgets.QLabel(self.groupBox_2)
        self.label_4.setGeometry(QtCore.QRect(38, 65, 153, 24))
        self.label_4.setObjectName("label_4")
        self.label_4.setText("kappa y")

        self.leBeta = QtWidgets.QLineEdit(self.groupBox_2)
        self.leBeta.setGeometry(QtCore.QRect(190, 105, 48, 24))
        self.leBeta.setText(str(beta))
        self.leBeta.setObjectName("leBeta")

        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(38, 105, 153, 24))
        self.label_5.setObjectName("label_5")
        self.label_5.setText("beta")

        self.leWinSize = QtWidgets.QLineEdit(self.groupBox_2)
        self.leWinSize.setGeometry(QtCore.QRect(190, 145, 48, 24))
        self.leWinSize.setText(str(win_size))
        self.leWinSize.setObjectName("leWinSize")

        self.label_6 = QtWidgets.QLabel(self.groupBox_2)
        self.label_6.setGeometry(QtCore.QRect(38, 145, 153, 24))
        self.label_6.setObjectName("label_6")
        self.label_6.setText("window size")

        self.leSigma = QtWidgets.QLineEdit(self.groupBox_2)
        self.leSigma.setGeometry(QtCore.QRect(190, 185, 48, 24))
        self.leSigma.setText(str(sigma))
        self.leSigma.setObjectName("leSigma")

        self.label_7 = QtWidgets.QLabel(self.groupBox_2)
        self.label_7.setGeometry(QtCore.QRect(38, 185, 153, 24))
        self.label_7.setObjectName("label_7")
        self.label_7.setText("sigma")

        self.leAreaTol = QtWidgets.QLineEdit(self.groupBox_2)
        self.leAreaTol.setGeometry(QtCore.QRect(190, 225, 48, 24))
        self.leAreaTol.setText(str(areatolerance))
        self.leAreaTol.setObjectName("leAreaTol")

        self.label_8 = QtWidgets.QLabel(self.groupBox_2)
        self.label_8.setGeometry(QtCore.QRect(38, 225, 153, 24))
        self.label_8.setObjectName("label_8")
        self.label_8.setText("area tolerance")

        self.buttonBox.rejected.connect(Dialog.reject)
        self.buttonBox.accepted.connect(Dialog.accept)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def parameters(self):
        PJSPlugin.kappa_x = float(self.leKappaX.text())
        PJSPlugin.kappa_y = float(self.leKappaY.text())
        PJSPlugin.sigma_gradient = float(self.leSigma.text())
        PJSPlugin.beta = float(self.leBeta.text())
        PJSPlugin.window_size = int(self.leWinSize.text())
        PJSPlugin.area_tolerance = float(self.leAreaTol.text())

        theparameters = {'first': self.sbFirst.value() - 1, 'last': self.sbLast.value(), 
                        'kappa_x': PJSPlugin.kappa_x, 'kappa_y': PJSPlugin.kappa_y,
                        'beta': PJSPlugin.beta, 'window_size': PJSPlugin.window_size, 
                        'sigma_gradient': PJSPlugin.sigma_gradient, 'area_tolerance': PJSPlugin.area_tolerance}

        return theparameters

    def MEDUSASegmentation(self, parameters, theslices):
        alpha = 3 
        gamma = 1

        #step 1: If the initial curves contain few points interpolate initial coordinates to obtain more points
        thepoly = self.pjs.polylines[parameters['first']][0]
        x = []
        y = []

        if len(thepoly) < 60:
            for i in range(len(thepoly)):
                firstpoint = thepoly[i]
                secondpoint = thepoly[(i+1)%len(thepoly)]
                numnew = int(((firstpoint.x() - secondpoint.x())**2 + (firstpoint.y() - secondpoint.y())**2)**0.5) 
                
                newx = numpy.linspace(firstpoint.x(), secondpoint.x(), numnew) #there nust be a better way to do this i'm so tired
                newy = numpy.linspace(firstpoint.y(), secondpoint.y(), numnew)
                
                x.extend(newx)
                y.extend(newy)


        else:
            x = [point.x() for point in thepoly]
            y = [point.y() for point in thepoly]
        
        x = numpy.array(x)
        y = numpy.array(y)

        print("x is", x)
        print("y is", y)

        #ok now the difficult, iterative part
        for i in range(theslices.size):
            curslice = theslices[i]
            img = self.pjs.slices[curslice]

            if i != 0: #step 2: apply optical flow to propagate the snake to next timepoints
                lastslice = theslices[i-1]

                Xflow, Yflow, _, _ = rimutils.flow(self.pjs.slices[lastslice], img,
                                            numpy.array(list(zip(x, y))), parameters['window_size'])
                x = x + Xflow
                y = y + Yflow

            #step 3: snakes implementation
            #build matrix P that will be used to find new coordinates
            N = len(x)
            a = gamma*(2*alpha+6*parameters['beta'])+1
            b = gamma*(-alpha-4*parameters['beta'])
            c = gamma*parameters['beta']
            print("P-parameters", N, a, b, c)

            P = numpy.diag(numpy.array([a]*N))
            P = P + numpy.diag(numpy.array([b]*(N-1)), 1) + numpy.diag([b], -N+1)
            P = P + numpy.diag(numpy.array([b]*(N-1)), -1) + numpy.diag([b], N-1)
            P = P + numpy.diag(numpy.array([c]*(N-2)), 2) + numpy.diag([c,c],-N+2)
            P = P + numpy.diag(numpy.array([c]*(N-2)), -2) + numpy.diag([c,c], N-2)

            try:
                P = numpy.linalg.inv(P) #i have no idea if this matrix is always invertible or not. maybe i need to read up on my linalg
            except numpy.linalg.LinAlgError:
                print("P not invertible")
                return False

            print("P is", P)

            #compute external force
            f = numpy.gradient(ndimage.gaussian_gradient_magnitude(img, sigma=parameters['sigma_gradient']))
            
            maxy, maxx = f[0].shape
            endsnakes = False
            areaSnakePrev = 0
            contadorError = 0

            while not endsnakes:
                #get the value of external force at the sname's control points
                contadorError += 1
                try:
                    interpXfield = interpolate.RectBivariateSpline(numpy.arange(maxy), numpy.arange(maxx), f[1])
                    fex = interpXfield(y, x, grid = False)
                except Exception as e:
                    print("i have no idea what this exception means x")
                    print(e)
                    return False
                
                try:
                    interpYfield = interpolate.RectBivariateSpline(numpy.arange(maxy), numpy.arange(maxx), f[0])
                    fey = interpYfield(y, x, grid = False)                
                except Exception as e:
                    print("i have no idea what this exception means y")
                    print(e)
                    return False

                print("fex is 1", fex)
                print("fey is 1", fey)
                
                #calculate balloon force. this has something to do with normals 
                #to the polygon, which i actually kind of understand
                if parameters['kappa_x'] != 0 or parameters['kappa_y'] != 0:
                    coords = numpy.array(list(zip(x, y)))

                    b = numpy.roll(coords, -1, axis=0) - numpy.roll(coords, 1, axis=0)
                    m = (numpy.sum(b**2, axis = 1))**0.5

                    m[m == 0] = 1
                    bx = numpy.divide(b[:, 0], m)
                    by = numpy.divide(-b[:, 1], m)

                    #compute a point of the director vector
                    x_v = (x + 5*bx)[::20]
                    y_v = (y + 5*by)[::20]

                    #compute if the vector points inside or outside the polygon
                    p = path.Path(coords)
                    inpolygon = p.contains_points(numpy.array(list(zip(x_v,y_v))))

                    if numpy.count_nonzero(inpolygon) > len(inpolygon)/2:
                        bx = -bx
                        by = -by

                    #add balloon force to external force
                    fex += parameters['kappa_x'] * bx
                    fey += parameters['kappa_y'] * by
                
                #move control points
                x = numpy.matmul(P, x + gamma*fex)
                y = numpy.matmul(P, y + gamma*fey)

                print("x is 2", x)
                print("y is 2", y)

                #measure area inside curve to stop algorithm
                thepolyline = QtGui.QPolygonF()
                for xpoint, ypoint in zip(x, y):
                    thepolyline.append(QtCore.QPointF(xpoint, ypoint))
                
                curarea = rpolyline.RPolyline(thepolyline).area()
                if abs(curarea - areaSnakePrev) < parameters['area_tolerance']:
                    endsnakes = True
                
                areaSnakePrev = curarea
                print(contadorError)
            
            self.pjs.addPolyline(list(zip(x, y)), curslice)

            #step 4: subsample the snake and apply livewire
            cpX = x[::8]
            cpY = y[::8]
            

            return True

            #self.pjs.addPolyline(list(zip(x, y)), curslice)

            #step 3: snakes implementation

            # haha nice try. maybe play w/ the parameters more in jupyter or something?
            # or just steal a notebook and program some sliders ig

            # initsnake = numpy.array(list(zip(x, y)))
            # newsnake = segmentation.active_contour(img, initsnake, alpha=alpha, beta=parameters['beta'], gamma=gamma)
            # self.pjs.addPolyline(newsnake, curslice)
            # x = newsnake[:, 0]
            # y = newsnake[:, 1]



            

